<!DOCTYPE html>
<html>
<head>
	<title>Game studio</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Bitter:400,700&amp;subset=latin-ext" rel="stylesheet">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="/resources/demos/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css" />
		<script>
		$( function() {
			$( "#menu" ).menu();
		} );
		</script>
    </head>
    <body>
        <header class="container">
            <button id=" login" type="button" class="btn-lg btn-primary float-right" ><a href="../registration/login.php">Login</button></a>
            <div class="row">
                <div class="col-sm-10 text-center">
                    <nav>
                        <ul class="inline">
                        <input class="search_game" type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for the games.." title="Type in a name">
                            <li class="list-inline-item"><a href="../home/home.php">Home</a></li>
                            <li class="list-inline-item"><a href="../games/index.php">Games</a></li>
                            <li class="list-inline-item"><a href="../contacts/contacts.php">Contacts</a></li>
                            <li class="list-inline-item"><a href="../news/news.php">News</a></li>
                        </ul>
                    </nav>
                </div>                
			</div>
		</header>