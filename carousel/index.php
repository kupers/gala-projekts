<?php 
include("../layout/header.php");
?>
<div id="Carousel" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#Carousel" data-slide-to="0" class="active"></li>
    <li data-target="#Carousel" data-slide-to="1"></li>
    <li data-target="#Carousel" data-slide-to="2"></li>
    <li data-target="#Carousel" data-slide-to="3"></li>
  </ol>
  <div class="carousel-inner" role="listbox">   
    <div class="item active">
      <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTQj-D_YiTCcg4cClTDyvWCiq04_cFd-Tqmk_pV4zXIylSgcBZG" alt="Need for Speed">
    </div>
    <div class="item">
      <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTCQqsseG1ZQpYnMPnUQZpIvitDu0l2Ypbrsvf1ZkpEzYIaK2Ga" alt="Cs go">
    </div>
    <div class="item">
      <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQajL_JnMObJLysi2jJT-GSmxq0w1FzQbU9hDsg8clftL7pulRUKw" alt="Dota 2">
    </div>
    <div class="item">
      <img src="https://i.pinimg.com/originals/ba/15/a5/ba15a584ca7d7265bd6659f86bc5506c.jpg" alt="Skyrim">
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#Carousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#Carousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<?php include("../layout/footer.php");?>